let BMWAndAudi = [];
if (!Array.isArray(inventory) || inventory.length == 0) {
    return inventory = [];
}
function problem6(inventory) {
    for (let i = 0; i < inventory.length; i++) {
        if (inventory[i].car_make === "BMW" || inventory[i].car_make === "Audi") {
            BMWAndAudi.push(inventory[i]);
        }
    }
    return BMWAndAudi;
}
module.exports = problem6;