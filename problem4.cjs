let carYears = [];
function problem4(inventory) {
    if(!Array.isArray(inventory) || inventory.length ==0) 
    {
        return inventory=[];
    }
    for (let i = 0; i < inventory.length; i++) {
        carYears.push(inventory[i].car_year);
    }
    return carYears;
}
module.exports = problem4;