function problem3(inventory) {
    if(!Array.isArray(inventory) || inventory.length ==0) 
    {
        return inventory=[];
    }
    let carModels = [];
    for (let i = 0; i < inventory.length; i++) {
        carModels.push(inventory[i].car_model);
    }
    carModels.sort();
    return carModels;
}
module.exports = problem3;