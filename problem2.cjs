function problem2(inventory) {
    if(!Array.isArray(inventory) || inventory.length ==0) 
    {
        return inventory=[];
    }

    let lastCar = 0;
    lastCar = inventory.length - 1;
    return `Last car is a ${inventory[lastCar].car_make} ${inventory[lastCar].car_model}`;
}

module.exports = problem2;