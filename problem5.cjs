var problem4 = require('./problem4.cjs');
if (!Array.isArray(inventory) || inventory.length == 0) {
    return inventory = [];
}
function problem5(inventory) {
    var carYears = problem4(inventory);
    let oldCars = [];
    for (let i = 0; i < carYears.length; i++) {
        if (carYears[i] < 2000) {
            oldCars.push(carYears[i]);
        }
    }
    return oldCars;
}
module.exports = problem5;